#!/usr/bin/env python3

#
# This file is subject to the terms and conditions defined in
# file 'LICENSE.txt', which is part of this source code package.
#

ITEMS = {
    ################################################################################
    # Ahn'Qiraj
    ################################################################################

    # Trash drops
    "Anubisath Warhammer" : ["AQ", "AQ_trash"],
    "Garb of Royal Ascension" : ["AQ", "AQ_trash"],
    "Gloves of the Immortal" : ["AQ", "AQ_trash"],
    "Neretzek, the Blood Drinker" : ["AQ", "AQ_trash"],
    "Ritssyn's Ring of Chaos" : ["AQ", "AQ_trash"],
    "Shard of the Fallen Star" : ["AQ", "AQ_trash"],

    # Shared drops
    "Imperial Qiraji Armaments" : ["AQ", "Skeram", "Trio", "Sartura", "Fankriss", "Viscidus", "Huhuran", "Twins", "Ouro"],
    "Imperial Qiraji Regalia" : ["AQ", "Skeram", "Trio", "Sartura", "Fankriss", "Viscidus", "Huhuran", "Twins", "Ouro"],
    "Qiraji Bindings of Command" : ["AQ", "Viscidus", "Huhuran"],
    "Qiraji Bindings of Dominance" : ["AQ", "Viscidus", "Huhuran"],

    # Skeram
    "Amulet of Foul Warding" : ["AQ", "Skeram"],
    "Barrage Shoulders" : ["AQ", "Skeram"],
    "Beetle Scaled Wristguards" : ["AQ", "Skeram"],
    "Boots of the Fallen Prophet" : ["AQ", "Skeram"],
    "Boots of the Redeemed Prophecy" : ["AQ", "Skeram"],
    "Boots of the Unwavering Will" : ["AQ", "Skeram"],
    "Breastplate of Annihilation" : ["AQ", "Skeram"],
    "Breastplate of Annihilation" : ["AQ", "Skeram"],
    "Cloak of Concentrated Hatred" : ["AQ", "Skeram"],
    "Cloak of Concentrated Hatred" : ["AQ", "Skeram"],
    "Hammer of Ji'zhi" : ["AQ", "Skeram"],
    "Leggings of Immersion" : ["AQ", "Skeram"],
    "Pendant of the Qiraji Guardian" : ["AQ", "Skeram"],
    "Ring of Swarming Thought" : ["AQ", "Skeram"],
    "Staff of the Qiraji Prophets" : ["AQ", "Skeram"],

    # Trio (Vem, Kri, Yauj)
    "Angelista's Charm" : ["AQ", "Trio", "Vem"],
    "Angelista's Touch" : ["AQ", "Trio", "Kri", "Vem", "Yauj"],
    "Bile-Covered Gauntlets" : ["AQ", "Trio", "Yauj"],
    "Boots of the Fallen Hero" : ["AQ", "Trio", "Vem"],
    "Cape of the Trinity" : ["AQ", "Trio", "Kri", "Yauj", "Vem"],
    "Gloves of Ebru" : ["AQ", "Trio", "Vem"],
    "Guise of the Devourer" : ["AQ", "Trio", "Kri", "Vem", "Yauj"],
    "Mantle of Phrenic Power" : ["AQ", "Trio", "Yauj"],
    "Mantle of the Desert Crusade" : ["AQ", "Trio", "Yauj"],
    "Mantle of the Desert's Fury" : ["AQ", "Trio", "Yauj"],
    "Ooze-ridden Gauntlets" : ["AQ", "Trio", "Vem"],
    "Petrified Scarab" : ["AQ", "Trio", "Kri"],
    "Ring of the Devoured" : ["AQ", "Trio", "Kri"],
    "Robes of the Triumvirate" : ["AQ", "Trio", "Kri", "Vem", "Yauj"],
    "Ternary Mantle" : ["AQ", "Trio", "Kri", "Vem", "Yauj"],
    "Triad Girdle" : ["AQ", "Trio", "Kri", "Vem", "Yauj"],
    "Ukko's Ring of Darkness" : ["AQ", "Trio", "Yauj"],
    "Vest of Swift Execution" : ["AQ", "Trio", "Kri"],
    "Wand of Qiraji Nobility" : ["AQ", "Trio", "Kri"],

    # Sartura
    "Badge of the Swarmguard" : ["AQ", "Sartura"],
    "Creeping Vine Helm" : ["AQ", "Sartura"],
    "Gauntlets of Steadfast Determination" : ["AQ", "Sartura"],
    "Gloves of Enforcement" : ["AQ", "Sartura"],
    "Leggings of the Festering Swarm" : ["AQ", "Sartura"],
    "Legplates of Blazing Light" : ["AQ", "Sartura"],
    "Necklace of Purity" : ["AQ", "Sartura"],
    "Recomposed Boots" : ["AQ", "Sartura"],
    "Robes of the Battleguard" : ["AQ", "Sartura"],
    "Sartura's Might" : ["AQ", "Sartura"],
    "Scaled Leggings of Qiraji Fury" : ["AQ", "Sartura"],
    "Silithid Claw" : ["AQ", "Sartura"],
    "Thick Qirajihide Belt" : ["AQ", "Sartura"],

    # Fankriss
    "Ancient Qiraji Ripper" : ["AQ", "Fankriss"],
    "Barb of the Sand Reaver" : ["AQ", "Fankriss"],
    "Barbed Choker" : ["AQ", "Fankriss"],
    "Cloak of Untold Secrets" : ["AQ", "Fankriss"],
    "Fetish of the Sand Reaver" : ["AQ", "Fankriss"],
    "Hive Tunneler's Boots" : ["AQ", "Fankriss"],
    "Libram of Grace" : ["AQ", "Fankriss"],
    "Mantle of Wicked Revenge" : ["AQ", "Fankriss"],
    "Pauldrons of the Unrelenting" : ["AQ", "Fankriss"],
    "Robes of the Guardian Saint" : ["AQ", "Fankriss"],
    "Scaled Sand Reaver Leggings" : ["AQ", "Fankriss"],
    "Silithid Carapace Chestguard" : ["AQ", "Fankriss"],
    "Totem of Life" : ["AQ", "Fankriss"],

    # Viscidus
    "Gauntlets of Kalimdor" : ["AQ", "Viscidus"],
    "Gauntlets of the Righteous Champion" : ["AQ", "Viscidus"],
    "Idol of Health" : ["AQ", "Viscidus"],
    "Ring of the Qiraji Fury" : ["AQ", "Viscidus"],
    "Scarab Brooch" : ["AQ", "Viscidus"],
    "Sharpened Silithid Femur" : ["AQ", "Viscidus"],
    "Slime-coated Leggings" : ["AQ", "Viscidus"],

    # Huhuran
    "Cloak of the Golden Hive" : ["AQ", "Huhuran"],
    "Gloves of the Messiah" : ["AQ", "Huhuran"],
    "Hive Defiler Wristguards" : ["AQ", "Huhuran"],
    "Huhuran's Stinger" : ["AQ", "Huhuran"],
    "Ring of the Martyr" : ["AQ", "Huhuran"],
    "Wasphide Gauntlets" : ["AQ", "Huhuran"],

    # Twin emperors
    "Amulet of Vek'nilash" : ["AQ", "Twins", "Vek'nilash"],
    "Belt of the Fallen Emperor" : ["AQ", "Twins", "Vek'nilash"],
    "Boots of Epiphany" : ["AQ", "Twins", "Vek'lor"],
    "Bracelets of Royal Redemption" : ["AQ", "Twins", "Vek'nilash"],
    "Gloves of the Hidden Temple" : ["AQ", "Twins", "Vek'nilash"],
    "Grasp of the Fallen Emperor" : ["AQ", "Twins", "Vek'nilash"],
    "Kalimdor's Revenge" : ["AQ", "Twins", "Vek'nilash"],
    "Qiraji Execution Bracers" : ["AQ", "Twins", "Vek'lor"],
    "Regenerating Belt of Vek'nilash" : ["AQ", "Twins", "Vek'nilash"],
    "Ring of Emperor Vek'lor" : ["AQ", "Twins", "Vek'lor"],
    "Royal Qiraji Belt" : ["AQ", "Twins", "Vek'lor"],
    "Royal Scepter of Vek'lor" : ["AQ", "Twins", "Vek'lor"],
    "Vek'lor's Diadem" : ["AQ", "Twins", "Vek'lor"],
    "Vek'lor's Gloves of Devastation" : ["AQ", "Twins", "Vek'lor"],
    "Vek'nilash's Circlet" : ["AQ", "Twins", "Vek'nilash"],

    # Ouro
    "Burrower Bracers" : ["AQ", "Ouro"],
    "Don Rigoberto's Lost Hat" : ["AQ", "Ouro"],
    "Jom Gabbar" : ["AQ", "Ouro"],
    "Larvae of the Great Worm" : ["AQ", "Ouro"],
    "Ouro's Intact Hide" : ["AQ", "Ouro"],
    "Skin of the Great Sandworm" : ["AQ", "Ouro"],
    "The Burrower's Shell" : ["AQ", "Ouro"],
    "Wormscale Blocker" : ["AQ", "Ouro"],

    # C'Thun
    "Belt of Never-ending Agony" : ["AQ", "C'Thun"],
    "Carapace of the Old God" : ["AQ", "C'Thun"],
    "Cloak of Clarity" : ["AQ", "C'Thun"],
    "Cloak of the Devoured" : ["AQ", "C'Thun"],
    "Dark Edge of Insanity" : ["AQ", "C'Thun"],
    "Dark Storm Gauntlets" : ["AQ", "C'Thun"],
    "Death's Sting" : ["AQ", "C'Thun"],
    "Eye of C'Thun" : ["AQ", "C'Thun"],
    "Eyestalk Waist Cord" : ["AQ", "C'Thun"],
    "Gauntlets of Annihilation" : ["AQ", "C'Thun"],
    "Grasp of the Old God" : ["AQ", "C'Thun"],
    "Husk of the Old God" : ["AQ", "C'Thun"],
    "Mark of C'Thun" : ["AQ", "C'Thun"],
    "Ring of the Godslayer" : ["AQ", "C'Thun"],
    "Scepter of the False Prophet" : ["AQ", "C'Thun"],
    "Vanquished Tentacle of C'Thun" : ["AQ", "C'Thun"],

    ################################################################################
    # Naxxramas
    ################################################################################

    # Trash
    "Pauldrons of Elemental Fury" : ["Naxxramas", "Naxxramas_trash"],
    "Necro-Knight's Garb" : ["Naxxramas", "Naxxramas_trash"],
    "Ghoul Skin Tunic" : ["Naxxramas", "Naxxramas_trash"],
    "Girdle of Elemental Fury" : ["Naxxramas", "Naxxramas_trash"],
    "Leggings of Elemental Fury" : ["Naxxramas", "Naxxramas_trash"],
    "Ring of the Eternal Flame" : ["Naxxramas", "Naxxramas_trash"],
    "Stygian Buckler" : ["Naxxramas", "Naxxramas_trash"],
    "Harbinger of Doom" : ["Naxxramas", "Naxxramas_trash"],
    "Misplaced Servo Arm" : ["Naxxramas", "Naxxramas_trash"],

    # Shared boss loot
    "Atiesh, Greatstaff of the Guardian" : ["Naxxramas", "Naxxramas_trash", "Anub'Rekhan", "Faerlina", "Maexxna", "Noth", "Heigan", "Loatheb", "Razuvious", "Gothik", "Four Horsemen", "Patchwerk", "Grobbulus", "Gluth", "Thaddius", "Sapphiron", "Kel'Thuzad"],
    "Desecrated Belt" : ["Naxxramas", "Gluth", "Heigan", "Noth"],
    "Desecrated Bindings" : ["Naxxramas", "Anub'Rekhan", "Faerlina", "Gluth"],
    "Desecrated Boots" : ["Naxxramas", "Gluth", "Gothik", "Razuvious"],
    "Desecrated Bracers" : ["Naxxramas", "Anub'Rekhan", "Faerlina", "Gluth"],
    "Desecrated Girdle" : ["Naxxramas", "Gluth", "Heigan", "Noth"],
    "Desecrated Pauldrons" : ["Naxxramas", "Gluth", "Grobbulus", "Patchwerk"],
    "Desecrated Sabatons" : ["Naxxramas", "Gluth", "Gothik", "Razuvious"],
    "Desecrated Sandals" : ["Naxxramas", "Gluth", "Gothik", "Razuvious"],
    "Desecrated Shoulderpads" : ["Naxxramas", "Gluth", "Grobbulus", "Patchwerk"],
    "Desecrated Spaulders" : ["Naxxramas", "Gluth", "Grobbulus", "Patchwerk"],
    "Desecrated Waistguard" : ["Naxxramas", "Gluth", "Heigan", "Noth"],
    "Desecrated Wristguards" : ["Naxxramas", "Anub'Rekhan", "Faerlina", "Gluth"],

    # Anub'Rekhan
    "Band of Unanswered Prayers" : ["Naxxramas", "Anub'Rekhan"],
    "Touch of Frost" : ["Naxxramas", "Anub'Rekhan"],
    "Gem of Nerubis" : ["Naxxramas", "Anub'Rekhan"],
    "Cryptfiend Silk Cloak" : ["Naxxramas", "Anub'Rekhan"],
    "Wristguards of Vengeance" : ["Naxxramas", "Anub'Rekhan"],

    # Faerlina
    "Polar Shoulder Pads" : ["Naxxramas", "Faerlina"],
    "Icebane Pauldrons" : ["Naxxramas", "Faerlina"],
    "Malice Stone Pendant" : ["Naxxramas", "Faerlina"],
    "The Widow's Embrace" : ["Naxxramas", "Faerlina"],
    "Widow's Remorse" : ["Naxxramas", "Faerlina"],

    # Maexxna
    "Kiss of the Spider" : ["Naxxramas", "Maexxna"],
    "Pendant of Forgotten Names" : ["Naxxramas", "Maexxna"],
    "Crystal Webbed Robe" : ["Naxxramas", "Maexxna"],
    "Wraith Blade" : ["Naxxramas", "Maexxna"],
    "Maexxna's Fang" : ["Naxxramas", "Maexxna"],
    "Desecrated Gloves" : ["Naxxramas", "Maexxna"],
    "Desecrated Handguards" : ["Naxxramas", "Maexxna"],
    "Desecrated Gauntlets" : ["Naxxramas", "Maexxna"],

    # Noth
    "Cloak of the Scourge" : ["Naxxramas", "Noth"],
    "Noth's Frigid Heart" : ["Naxxramas", "Noth"],
    "Band of the Inevitable" : ["Naxxramas", "Noth"],
    "Hailstone Band" : ["Naxxramas", "Noth"],
    "Hatchet of Sundered Bone" : ["Naxxramas", "Noth"],
    "Libram of Light" : ["Naxxramas", "Noth"],
    "Totem of Flowing Water" : ["Naxxramas", "Noth"],

    # Heigan
    "Legplates of Carnage" : ["Naxxramas", "Heigan"],
    "Icy Scale Coif" : ["Naxxramas", "Heigan"],
    "Preceptor's Hat" : ["Naxxramas", "Heigan"],
    "Icebane Helmet" : ["Naxxramas", "Heigan"],
    "Necklace of Necropsy" : ["Naxxramas", "Heigan"],

    # Loatheb
    "Band of Unnatural Forces" : ["Naxxramas", "Loatheb"],
    "Ring of Spiritual Fervor" : ["Naxxramas", "Loatheb"],
    "Loatheb's Reflection" : ["Naxxramas", "Loatheb"],
    "The Eye of Nerub" : ["Naxxramas", "Loatheb"],
    "Brimstone Staff" : ["Naxxramas", "Loatheb"],
    "Desecrated Leggings" : ["Naxxramas", "Loatheb"],
    "Desecrated Legguards" : ["Naxxramas", "Loatheb"],
    "Desecrated Legplates" : ["Naxxramas", "Loatheb"],

    # Razuvious	
    "Girdle of the Mentor" : ["Naxxramas", "Razuvious"],
    "Wand of the Whispering Dead" : ["Naxxramas", "Razuvious"],
    "Signet of the Fallen Defender" : ["Naxxramas", "Razuvious"],
    "Idol of Longevity" : ["Naxxramas", "Razuvious"],
    "Veil of Eclipse" : ["Naxxramas", "Razuvious"],
    "Iblis, Blade of the Fallen Seraph" : ["Naxxramas", "Razuvious"],

    # Gothik
    "Glacial Headdress" : ["Naxxramas", "Gothik"],
    "The Soul Harvester's Bindings" : ["Naxxramas", "Gothik"],
    "Sadist's Collar" : ["Naxxramas", "Gothik"],
    "Boots of Displacement" : ["Naxxramas", "Gothik"],
    "Polar Helmet" : ["Naxxramas", "Gothik"],

    # 4 hm	
    "Corrupted Ashbringer" : ["Naxxramas", "Four Horsemen"],
    "Seal of the Damned" : ["Naxxramas", "Four Horsemen"],
    "Warmth of Forgiveness" : ["Naxxramas", "Four Horsemen"],
    "Leggings of Apocalypse" : ["Naxxramas", "Four Horsemen"],
    "Maul of the Redeemed Crusader" : ["Naxxramas", "Four Horsemen"],
    "Soulstring" : ["Naxxramas", "Four Horsemen"],
    "Desecrated Breastplate" : ["Naxxramas", "Four Horsemen"],
    "Desecrated Robe" : ["Naxxramas", "Four Horsemen"],
    "Desecrated Tunic" : ["Naxxramas", "Four Horsemen"],

    # Patchwerk	
    "Cloak of Suturing" : ["Naxxramas", "Patchwerk"],
    "Band of Reanimation" : ["Naxxramas", "Patchwerk"],
    "Wand of Fates" : ["Naxxramas", "Patchwerk"],
    "The Plague Bearer" : ["Naxxramas", "Patchwerk"],
    "Severance" : ["Naxxramas", "Patchwerk"],

    # Grobbulus
    "Icy Scale Spaulders" : ["Naxxramas", "Grobbulus"],
    "The End of Dreams" : ["Naxxramas", "Grobbulus"],
    "Glacial Mantle" : ["Naxxramas", "Grobbulus"],
    "Toxin Injector" : ["Naxxramas", "Grobbulus"],
    "Midnight Haze" : ["Naxxramas", "Grobbulus"],

    # Gluth
    "Claymore of Unholy Might" : ["Naxxramas", "Gluth"],
    "Gluth's Missing Collar" : ["Naxxramas", "Gluth"],
    "Digested Hand of Power" : ["Naxxramas", "Gluth"],
    "Death's Bargain" : ["Naxxramas", "Gluth"],
    "Rime Covered Mantle" : ["Naxxramas", "Gluth"],

    # Thaddius	
    "Desecrated Helmet" : ["Naxxramas", "Thaddius"],
    "Desecrated Headpiece" : ["Naxxramas", "Thaddius"],
    "Desecrated Circlet" : ["Naxxramas", "Thaddius"],
    "Leggings of Polarity" : ["Naxxramas", "Thaddius"],
    "Spire of Twilight" : ["Naxxramas", "Thaddius"],
    "Eye of Diminution" : ["Naxxramas", "Thaddius"],
    "Plated Abomination Ribcage" : ["Naxxramas", "Thaddius"],
    "The Castigator" : ["Naxxramas", "Thaddius"],

    # Sapphiron    
#    "Might of the Scourge" : ["Naxxramas", "Sapphiron"],
#    "Power of the Scourge" : ["Naxxramas", "Sapphiron"],
    "Eye of the Dead" : ["Naxxramas", "Sapphiron"],
    "Shroud of Dominion" : ["Naxxramas", "Sapphiron"],
    "Claw of the Frost Wyrm" : ["Naxxramas", "Sapphiron"],
    "The Face of Death" : ["Naxxramas", "Sapphiron"],
    "Sapphiron's Right Eye" : ["Naxxramas", "Sapphiron"],
    "Slayer's Crest" : ["Naxxramas", "Sapphiron"],
    "Glyph of Deflection" : ["Naxxramas", "Sapphiron"],
    "The Restrained Essence of Sapphiron" : ["Naxxramas", "Sapphiron"],
    "Cloak of the Necropolis" : ["Naxxramas", "Sapphiron"],
    "Sapphiron's Left Eye" : ["Naxxramas", "Sapphiron"],

    # Kel'Thuzad
    "The Phylactery of Kel'Thuzad" : ["Naxxramas", "Kel'Thuzad"],
    "Bonescythe Ring" : ["Naxxramas", "Kel'Thuzad"],
    "Gem of Trapped Innocents" : ["Naxxramas", "Kel'Thuzad"],
    "Might of Menethil" : ["Naxxramas", "Kel'Thuzad"],
    "Ring of Redemption" : ["Naxxramas", "Kel'Thuzad"],
    "Ring of the Dreadnaught" : ["Naxxramas", "Kel'Thuzad"],
    "Kingsfall" : ["Naxxramas", "Kel'Thuzad"],
    "Ring of the Earthshatterer" : ["Naxxramas", "Kel'Thuzad"],
    "Stormrage's Talisman of Seething" : ["Naxxramas", "Kel'Thuzad"],
    "Frostfire Ring" : ["Naxxramas", "Kel'Thuzad"],
    "Hammer of the Twisting Nether" : ["Naxxramas", "Kel'Thuzad"],
    "Ring of the Cryptstalker" : ["Naxxramas", "Kel'Thuzad"],
    "Soulseeker" : ["Naxxramas", "Kel'Thuzad"],
    "Doomfinger" : ["Naxxramas", "Kel'Thuzad"],
    "Ring of Faith" : ["Naxxramas", "Kel'Thuzad"],
    "Gressil, Dawn of Ruin" : ["Naxxramas", "Kel'Thuzad"],
    "Plagueheart Ring" : ["Naxxramas", "Kel'Thuzad"],
    "Ring of the Dreamwalker" : ["Naxxramas", "Kel'Thuzad"],
    "Shield of Condemnation" : ["Naxxramas", "Kel'Thuzad"],
    "Nerubian Slavemaker" : ["Naxxramas", "Kel'Thuzad"],
    "The Hungering Cold" : ["Naxxramas", "Kel'Thuzad"],
}

TANK_MITIG_ITEMS = [
    "Cloak of the Golden Hive",
    "Mark of C'Thun",
    "Pendant of the Qiraji Guardian",
    "Gluth's Missing Collar",
    "Pauldrons of the Unrelenting",
    "Cryptfiend Silk Cloak",
    "Gauntlets of Steadfast Determination",
    "Boots of the Unwavering Will",
    "Royal Qiraji Belt",
    "Hailstone Band",
    "Signet of the Fallen Defender",
    "Ring of Emperor Vek'lor",
    "Angelista's Touch",
    "Glyph of Deflection",
    "The Face of Death",
    "The Plague Bearer",
]

TANK_THREAT_ITEMS = [
    "Kiss of the Spider",
    "Girdle of the Mentor",
    "Vek'nilash's Circlet",
    "Carapace of the Old God",
    "Ouro's Intact Hide",
    "Qiraji Bindings of Command",
    "The Hungering Cold",
    "Death's Sting",
]

ORC_PENALTY = [
    "Gressil, Dawn of Ruin",
    "The Hungering Cold",
    "Iblis, Blade of the Fallen Seraph",
    "Widow's Remorse"
]
NON_ORC_PENALTY = [
    "Hatchet of Sundered Bone"
]

AQ_TAGS = ["AQ_trash", "Skeram", "Trio", "Sartura", "Fankriss", "Viscidus", "Huhuran", "Twins", "Ouro", "C'Thun"]
NAXX_TAGS = ["Naxxramas_trash", "Anub'Rekhan", "Faerlina", "Maexxna", "Noth", "Heigan", "Loatheb", "Razuvious", "Gothik", "Four Horsemen", "Patchwerk", "Grobbulus", "Gluth", "Thaddius", "Sapphiron", "Kel'Thuzad"]

def find_item(item_name):
    """Finds an item in the global item list defined in itemlist.py

    - `item_name' does not have to be sanitized.
    """
    item_name = item_name.strip().lower()
    for item in ITEMS:
        if item.strip().lower() == item_name:
            return [item, ITEMS[item]]
    return None


class Item(object):
    """Simple Item class used to sanitize item names."""

    def __init__(self, name):
        self.tainted_name = name.strip()
        self.name = None
        self.tags = None
        v = find_item(name)
        if v is not None:
            self.name, self.tags = v

    def exists(self):
        return self.name is not None
