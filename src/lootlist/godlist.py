#!/usr/bin/env python3

#
# This file is subject to the terms and conditions defined in
# file 'LICENSE.txt', which is part of this source code package.
#

import sys, os
import os.path
import argparse

from itemlist import *
import rules

import printer
import weakaura
import html

# Some shitty logging code
LOG_LEVEL=0

def log(logstr):
    if LOG_LEVEL > 0:
        print("LOG: %s" % (logstr))

def error(errstr, fatal=False):
    print("ERROR: %s" % (errstr), file=sys.stderr)
    if fatal:
        print(" -- FATAL --")
        raise RuntimeError

ERRS_ONCE = set()
def error_once(key, errstr, fatal=False):
    global ERRS_ONCE
    if key not in ERRS_ONCE:
        error(errstr, fatal)
        ERRS_ONCE.add(key)


class LootlistEntry(object):
    """A single lootlist entry, used to keep track of item scores"""

    def __init__(self, item, score):
        self.item = item
        self.original_score = score
        self.score = score
        self.missed = 0
        self.passed = 0
        self.acquired = False
        self.acquired_raid = None


class Character(object):
    """A character class which keeps track of a character progression."""

    def __init__(self, guild):
        self.guild = guild

        # Character name, capitalized
        self.name = ""

        # From OCS / NON_ORCS files, used to compute the axe/sword bonus
        self.is_orc_warrior = False
        self.is_meme_warrior = False

        # From the TANKS file, used for item bonuses
        self.is_tank = False

        # From ALTS files, used to compute alt characters penalty
        self.is_alt = False

        # This can change from raid to raid. It freezes the loot rules but still updates attendance
        self.is_apply = True

        # From ALUMNI files. Hides the character from the exports.
        self.is_alumni = False

        # Lootlist as a list of LootlistEntry
        self.lootlist = []
        self.acquired = []

        # For each raid week, log all raids attended, e.g.: [[week_number, 2]]
        self.attendance = []

        # Recomputed every raid reset based on self.attendance
        self.attendance_percentage = 0

        # Items missed during the current raid
        self.missed_items = [] # [Item]
        self.attended_full_instance = False

    def init_from_file(self, filepath):
        """Initializes lootlist from a '.char' file"""
        self.name = os.path.basename(filepath).split('.')[0].capitalize()

        with open(filepath) as f:
            contents = f.read()

        lines = contents.splitlines()
        for i, line in enumerate(lines):
            item = line.strip().lower()
            score = 50 - int(i / 2)

            if item:
                i = Item(item)
                if not i.exists():
                    error("Character [%s] references unknown item [%s]" % (self.name, line.strip()))

                self.lootlist.append(LootlistEntry(i, score))

    def _sort_lootlist(self):
        self.lootlist.sort(key=lambda x: x.score, reverse=True)

    def update_attendance(self):
        """Recomputes attendance percentage, and update lootlist scores if it changed."""
        perfect_attendance = self.guild.attendance_per_week

        # Check that a character hasn't done more raids than possible
        d = dict(perfect_attendance)
        for week in self.attendance:
            if week[0] not in d:
                error_once(str(week[0]) + 'notcleared', 'unknown ID %d' % (week[0]), fatal=True)
            elif week[1] > d[week[0]]:
                error("Character [%s] attendance for week %d: attended %d raids out of maximum %d" % (self.name, week[0], week[1], d[week[0]]), fatal=True)

        attendance_percentage = rules.compute_attendance_percentage(self.attendance, perfect_attendance)

        if attendance_percentage != self.attendance_percentage:
            # Recompute all items scores. We could just recompute the attendance bonus once since
            # it's the same everywhere, but meh.
            self.attendance_percentage = attendance_percentage
            self.update_lootlist()

    def update_lootlist(self):
        """Recomputes item scores for each lootlist entry."""
        for n in self.lootlist:
            self.compute_entry_score(n)
        self._sort_lootlist()

    def compute_entry_score(self, entry):
        """Computes an item score for a single entry using formulas from rules.py"""
        assigned_value = entry.original_score
        is_warrior = self.is_orc_warrior or self.is_meme_warrior
        entry.score = rules.compute_item_score(entry.item, entry.original_score,
                                               self.attendance_percentage,
                                               entry.missed, entry.passed,
                                               self.is_tank, is_warrior, self.is_orc_warrior, self.is_alt)

    def on_item_missed(self, item, raid):
        """Called when an item drops in a raid, but this character wasn't up for it"""
        if self.has_item_in_lootlist(item) and not self.is_apply:
            self.missed_items.append(item)

    def on_item_passed(self, item, raid):
        """Called when an item drops in a raid and this character was up for it but didn't get it."""
        if not self.is_apply:
            for entry in self.lootlist:
                if entry.item.name == item.name:
                    entry.passed += 1
                    self.compute_entry_score(entry)
                    self._sort_lootlist()
                    break # Only apply the bonus to the first occurence

    def on_item_acquired(self, item, raid):
        """Called when looting an item !"""
        for entry in self.lootlist:
            if entry.item.name == item.name:
                self.lootlist.remove(entry)
                self.acquired.append(entry)
                entry.acquired = True
                entry.acquired_raid = raid
                break

    def on_item_lost(self, item, raid):
        """Called when an item was for us, but staff lost it somehow..."""
        # Be lazy and act as if we passed twice to get a +2
        for entry in self.lootlist:
            if entry.item.name == item.name:
                entry.passed += 2
                self.compute_entry_score(entry)
                self._sort_lootlist()
                break # Only apply the bonus to the first occurence

    def on_raid_joined(self, raid, is_apply):
        """Called when joining a raid roster"""
        self.missed_items = []
        self.attended_full_instance = False
        if not self.is_apply and is_apply:
            error("Raid file [%s]: Character [%s] going from member to apply!" % (raid.raidid.filename, self.name))
        self.is_apply = is_apply

    def on_raid_left(self, raid):
        """Called when leaving a raid roster"""
        self.missed_items = []
        self.attended_full_instance = False

    def on_instance_started(self, raid):
        """Called when starting a new raid instance."""
        self.missed_items = []
        self.attended_full_instance = True

    def on_instance_cleared(self, raid):
        """Called when the current raid instance has been cleared."""
        if self.attended_full_instance:
            week = raid.raidid.week
            t = next((x for x in self.attendance if x[0] == week), [week, 0])
            if t not in self.attendance:
                self.attendance.append(t)
            t[1] += raid.attendance
            # TODO: Add some sanity checks (e.g. character didn't clean same instance twice per ID)

            # Add bonus for missed items
            for missed_item in self.missed_items:
                entries = self.get_lootlist_entries(missed_item)
                if entries:
                    # Only the top item gets the bonus
                    entries[0].missed += 1
                    self.compute_entry_score(entries[0])

            if self.missed_items:
                self._sort_lootlist()

        self.missed_items = []

    def get_lootlist_entries(self, item):
        return [n for n in self.lootlist if n.item.name == item.name]

    def has_item_in_lootlist(self, item):
        for n in self.lootlist:
            if n.item.name == item.name:
                return True
        return False

    def get_item_scores(self, item):
        return [n.score for n in self.get_lootlist_entries(item)]


class Raid(object):

    def __init__(self, raidid):
        self.raidid = raidid
        self.guild = raidid.guild
        self.attendance = -1
        self.instance = ""
        self.members = {} # {str : Character}
        self.started = False

    def character_left(self, name):
        name = name.capitalize()
        if name in self.members:
            char = self.members[name]
            if char is not None:
                char.on_raid_left(self)
            del self.members[name]
        else:
            error("Raid file [%s]: character [%s] left raid but never joined" % (self.raidid.filename, name))

    def character_joined(self, name, is_apply):
        name = name.capitalize()
        if name in self.members:
            error("Raid file [%s]: character [%s] joined raid but was already there" % (self.raidid.filename, name))
        else:
            char = self.guild.get_character(name)
            if char is None:
                pass
                # Spammy...
                # error_once(name + 'lootlist', "Raid file [%s]: character [%s] has no lootlist" % (self.raidid.filename, name))
            else:
                char.on_raid_joined(self, is_apply)
            self.members[name] = char

    def instance_started(self):
        if self.started:
            error("Raid file [%s]: instance %s already started" % (self.raidid.filename, self.instance), fatal=True)
        self.started = True
        for char in self.members.values():
            if char is not None:
                char.on_instance_started(self)

    def instance_cleared(self):
        if not self.started:
            error("Raid file [%s]: instance %s cleared, but no start line found (line starting with '=')" % (self.raidid.filename, self.instance), fatal=True)
        for char in self.members.values():
            if char is not None:
                char.on_instance_cleared(self)

    def process_loot(self, action, item, attrib):
        """
        Action is a string determining how to process the loot:
        '*' - Process the loot normally, updating item standings of everybody etc.
        '>' - Process the loot as if it got loot counciled in complete disregard of the lootlist system.
        '?' - Loot was lost but should have gone to `attrib'
        """
        if not item.exists():
            error('Raid file [%s]: Skipping unknown item [%s] for loot attributed to [%s]' % (self.raidid.filename, item.tainted_name, attrib))
            return

        passed = []
        missed = []
        winner = self.members[attrib] if attrib in self.members else None

        if winner is not None:
            scores = winner.get_item_scores(item)
            attrib_score = scores[0] if scores else 0
        else:
            attrib_score = 0
            if attrib and winner is None:
                if self.guild.get_character(attrib) is None:
                    if action == '*':
                        error('Raid file [%s]: Loot [%s] attributed to character [%s] without lootlist ! Everyone will receive a bonus' % (self.raidid.filename, item.name, attrib))
                else:
                    error('Raid file [%s]: Loot [%s] attributed to character [%s] who is not in the raid !' % (self.raidid.filename, item.name, attrib), fatal=True)

        # Find out who passed or missed the loot
        for char in self.members.values():
            if char is None:
                continue

            if char != winner:
                scores = char.get_item_scores(item)
                if scores:
                    # Everyone with a score >= to the winner must have
                    # passed or lost roll/council.
                    # Otherwise, they were just not up for that item yet.
                    if int(scores[0]) >= int(attrib_score):
                        passed.append(char)
                    else:
                        missed.append(char)

        if action in ['*', '?']:
            for char in passed:
                char.on_item_passed(item, self)

            for char in missed:
                char.on_item_missed(item, self)

        if winner is not None:
            if action == '?':
                winner.on_item_lost(item, self)
            else:
                winner.on_item_acquired(item, self)


class RaidID(object):

    def __init__(self, guild, filename):
        self.guild = guild
        self.filename = filename
        self.raids = []
        self.max_attendance = 0
        self.week = 0

    def process(self):
        with open(self.filename) as f:
            contents = f.read()

        filename = os.path.basename(self.filename).split('.')[0]
        self.week = int(filename)

        # Split sections
        lines = contents.splitlines()
        sections = []
        current_section = None
        for line in lines:
            stripped_line = line.split('#')[0].strip()

            if not stripped_line:
                continue

            if stripped_line.startswith('[') and stripped_line.endswith(']'):
                current_section = [stripped_line]
                sections.append(current_section)
            else:
                if current_section is None:
                    error("Raid file [%s]: line outside of section: [%s]" % (filepath, line), fatal=True)
                current_section.append(stripped_line)

        for section in sections:
            if section[0] == '[id]':
                self.process_header(section)
            else:
                self.process_raid(section)

    def process_header(self, section):
        for line in section[1:]:
            # No space allowed for the key=value lines, but we'll sanitize this later
            key, value = line.split('=')
            setattr(self, key, int(value))

    def process_raid(self, section):
        raid = Raid(self)
        header = section[0][1:-1] # Remove brackets
        # No space allowed for the key=value, but we'll sanitize this later
        attribs = header.split()
        raid.instance = attribs[0]
        for attr in attribs[1:]:
            key, value = attr.split('=')
            setattr(raid, key, int(value))

        if raid.attendance == -1:
            error("Raid file [%s]: instance [%s] has no attendance attribute, default to 1." % (self.filename, raid.instance))
            raid.attendance = 1

        for line in section[1:]:
            action = line[0]
            line = line[1:]

            if action in ['+', '-']:
                words = line.split()
                for name in words:
                    # names in brackets are applies
                    is_apply = False
                    if name.startswith('['):
                        assert(name.endswith(']'))
                        name = name[1:-1]
                        is_apply = True
                    if action == '+':
                        raid.character_joined(name, is_apply)
                    else:
                        raid.character_left(name)

            if action == '=':
                raid.instance_started()

            if action in ['*', '>', '?']:
                item, attrib = line.split(':')
                attrib = attrib.strip().capitalize()
                raid.process_loot(action, Item(item), attrib)

        raid.instance_cleared()

        self.raids.append(raid)


class Guild(object):

    def __init__(self):
        self.name = ""
        self.members = {} # Str: Character
        self.raids = [] # RaidID
        self.attendance_per_week = []

    def init_from_dir(self, path):
        log("Initializing Guild from [%s]:" % (path))
        files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
        charfiles = [f for f in files if f.endswith(".char")]
        raidfiles = [f for f in files if f.endswith(".raidid")]

        log("  - Reading character files...")
        for f in charfiles:
            log("    - Loading [%s]" % (f))
            char = Character(self)
            char.init_from_file(os.path.join(path, f))
            self.members[char.name] = char

        log("  - Reading raid files...")
        raidfiles.sort()
        for f in raidfiles:
            log("    - Loading [%s]" % (f))
            raid = RaidID(self, os.path.join(path, f))
            self.raids.append(raid)

        # Read special attribute files
        # TODO: merge all these in an ATTRIBUTES file
        spec_files = [['TANKS',    lambda c: setattr(c, 'is_tank', True)],
                      ['ALTS',     lambda c: setattr(c, 'is_alt', True)],
                      ['ORCS',     lambda c: setattr(c, 'is_orc_warrior', True)],
                      ['NON_ORCS', lambda c: setattr(c, 'is_meme_warrior', True)],
                      ['ALUMNI',   lambda c: setattr(c, 'is_alumni', True)]]

        for entry in spec_files:
            filename = entry[0]
            log("  - Loading %s..." % (filename) if filename in files else \
                  "  - No %s file" % (filename))
            if entry[0] not in files:
                continue

            filename = os.path.join(path, entry[0])
            with open(filename) as f:
                lines = f.read().splitlines()

            for line in lines:
                charname = line.strip().capitalize()
                char = self.get_character(charname)
                if char is not None:
                    entry[1](char)

        # update the lootlist for each character once to take the flags into account
        for char in self.members.values():
            char.update_lootlist()

    def get_character(self, name):
        name = name.capitalize()
        if name in self.members:
            return self.members[name]
        return None

    def process_raids(self, last_week=0):
        for raidid in self.raids:
            raidid.process()
            self.attendance_per_week.append([raidid.week, raidid.max_attendance])
            for char in self.members.values():
                char.update_attendance()

            if last_week and last_week == raidid.week:
                break


def main(argv):
    parser = argparse.ArgumentParser(description='Godfist lootlist manager')
    parser.add_argument(type=str, dest='guildpath', help='Guild directory')
    parser.add_argument('-c', type=str, nargs='+', dest='characters', help='Character filter. If only one character is specified, displays a detailed history.')
    parser.add_argument('-i', type=str, nargs='+', dest='items', help='Show items details and standings. If only one item is specified, displays a detailed history.')
    parser.add_argument('-t', type=str, nargs='+', dest='tags', help='Tag filter. See tag list.') # TODO: add tag list to help output
    parser.add_argument('-n', type=int, dest='count', default=-1, help='Only output N columns')
    parser.add_argument('-v', action='store_true', dest='verbose', default=False, help='Show logs')
    parser.add_argument('-q', action='store_true', dest='quiet', default=False, help='Loads and processes raids and quits without displaying anything. Useful for validation.')
    parser.add_argument('-a', action='store_true', dest='attendance', default=False, help='Show attendance')
    parser.add_argument('--html', action='store_true', dest='html', default=False, help='Outputs a global summary as HTML. All other options are ignored.')
    parser.add_argument('--html2', type=str, dest='html2_directory', help='Outputs a global summary as an HTML directory. All other options are ignored.')
    parser.add_argument('--wa', action='store_true', dest='lua', default=False, help='Outputs a weakaura of the lootlist. All other options are ignored.')

    args = parser.parse_args()

    if args.verbose:
        global LOG_LEVEL
        LOG_LEVEL=1

    guild = Guild()
    guild.init_from_dir(args.guildpath)
    guild.process_raids()

    if args.quiet:
        exit(0)

    # This is now the main use-case for this script. This will basically generate the full lootlist website.
    if args.html2_directory:
        html.export_html(guild, args.html2_directory)
        exit(0)

    if args.html:
        printer.display_global_html(guild)
        exit(0)

    if args.lua:
        weakaura.generate_weakaura(guild)
        exit(0)

    # Below are CLI-only outputs
    characters = guild.members.values()
    if args.characters:
        characters = []
        for name in args.characters:
            char = guild.get_character(name)
            if char is None:
                log('INFO: %s not found in guild' % (name))
            elif char not in characters:
                characters.append(char)

    tags = AQ_TAGS + NAXX_TAGS
    if args.tags:
        tags = args.tags
        # TODO ease command line by replacing "AQ" with AQ_TAGS etc

    items = list(map(Item, ITEMS.keys()))
    if args.items:
        # Overwrite tags
        tags = []
        items = []
        for name in args.items:
            item = Item(name)
            if item.exists():
                items.append(item)
            else:
                # act as a search
                for realname in ITEMS:
                    if name.lower() in realname.lower():
                        items.append(Item(realname))

    if len(characters) > 1 and len(items) > 1:
        if args.attendance:
            printer.display_attendance(guild, characters)
        printer.display_scores_per_tag(guild, tags, characters, items, args.count)
    elif len(characters) > 1:
        # TODO: printer.display_detailed_item(guild, tags, characters, items[0])
        printer.display_scores_per_tag(guild, tags, characters, items, args.count)
    else:
        printer.display_detailed_character(guild, tags, characters[0], items)

if __name__ == '__main__':
    main(sys.argv)
