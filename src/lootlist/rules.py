#!/usr/bin/env python3

#
# This file is subject to the terms and conditions defined in
# file 'LICENSE.txt', which is part of this source code package.
#

from itemlist import *

ATTENDANCE_LOOKBACK = 16
IGNORED_RAIDS = 2

MAX_ATTENDANCE_BONUS = 8
TANK_THREAT_BONUS = 3
TANK_MITIG_BONUS = 5
DUMB_WEAPON_PENALTY = -3

def apply_item_history_bonus(score, missed, passed):
    # An item receives a +0.4 bonus for each time it was missed
    # (i.e. the character was not up for it)
    score += (missed * 0.4)

    # If a character was up for an item but passed, it is a +1 bonus
    score += passed

    return score

def apply_attendance_bonus(score, attendance_percentage):
    # Linear scaling from 0 to MAX_ATTENDANCE_BONUS
    ratio = 100.0 / MAX_ATTENDANCE_BONUS
    return score + (attendance_percentage / ratio)

def apply_item_semantic_bonus(score, item, is_tank, is_dps_warrior, is_orc):
    # Apply tank bonus for threat/mitig
    if is_tank:
        if item.name in TANK_MITIG_ITEMS:
            score = score + TANK_MITIG_BONUS
        elif item.name in TANK_THREAT_ITEMS:
            score = score + TANK_THREAT_BONUS

    # Apply DPS warriors penalty for dumb weapon choice
    elif (is_dps_warrior and
          ((is_orc     and item.name in ORC_PENALTY) or
          (not is_orc and item.name in NON_ORC_PENALTY))):
        score = score + DUMB_WEAPON_PENALTY

    return score

def apply_alt_penalty(score, attendance_percentage):
    # Alts get a penalty that scales on their attendance, but
    # always receive at least a x0.9 penalty
    return score * min((attendance_percentage / 100.0), 0.9)

def compute_attendance_percentage(char_attendance, perfect_attendance):
    """Attendance list of the form: [(id, clears)]
    """
    lookback = perfect_attendance[-ATTENDANCE_LOOKBACK:]
    lookback_ids = [n[0] for n in lookback]
    total_raids = sum([n[1] for n in lookback])

    character_raids = sum(n[1] for n in char_attendance if n[0] in lookback_ids)

    if total_raids < ATTENDANCE_LOOKBACK:
        total_raids = ATTENDANCE_LOOKBACK

    return min((character_raids / (total_raids - IGNORED_RAIDS)) * 100.0, 100.0)

def compute_item_score(item, assigned_value, attendance_percentage,
                       missed_count, passed_count,
                       is_tank, is_dps_warrior, is_orc, is_alt):
    score = assigned_value
    score = apply_item_history_bonus(score, missed_count, passed_count)
    score = apply_attendance_bonus(score, attendance_percentage)
    score = apply_item_semantic_bonus(score, item, is_tank, is_dps_warrior, is_orc)

    if is_alt:
        score = apply_alt_penalty(score, attendance_percentage)

    return score
