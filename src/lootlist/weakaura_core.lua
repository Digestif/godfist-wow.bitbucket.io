--
-- This file is subject to the terms and conditions defined in
-- file 'LICENSE.txt', which is part of this source code package.
--

-- Godfist Lootlist Core WA. When updating this WA, use the game client to export the new version to wago.io
-- and paste the wagio.io link in html.py

-- TODO:
--   - Use a CSV string for the lootlist tables instead of a LUA array. It can be lazily parsed on
--     the first !loot command
--
--   - Add options on how to print results (e.g. in /raid /say etc.)

function(event, msg, sender, ...)

    local __VERSION__ = "0.2" -- Should match WA_VERSION in weakaura.py


    local selfName = UnitName("player") -- React only to self !loot to avoid grief until I add some options
    local channel = "RAID"
    local dest = nil

    local itemString = string.match(msg, "^!loot (.*)")

    if (event == "CHAT_MSG_WHISPER") then
        -- If request was a /w, only send results to raid if the command was !loot/r
        if (itemString == nil) then
            itemString = string.match(msg, "^!loot/r (.*)")
        else
            channel = "WHISPER"
            dest = sender
        end
    end

    selfName = selfName .. "-" .. "Amnennar" -- kek hack

    if (itemString ~= nil and (selfName == sender or event == "CHAT_MSG_WHISPER")) then
        local itemName, itemLink, quality, _, _, class, subclass, _, equipSlot, itemIcon, _, ClassID, SubClassID = GetItemInfo(itemString)

        if (aura_env.CORE_VERSION ~= __VERSION__) then
            print("  Core WA v" .. __VERSION__ )
            print("  Lootlist tables v" .. aura_env.CORE_VERSION)
            print("  Visit godfist-wow.bitbucket.io to update your WA.")
        end

        -- TODO: parse a CSV
        itemScores = aura_env.LOOTLIST_EN[itemName]
        if (itemScores == nil) then
            itemScores = aura_env.LOOTLIST_FR[itemName]
        end

        if (itemScores) then

            SendChatMessage(itemString .. " standings (lootlist " .. aura_env.LOOTLIST_MD5 .. "):", channel, nil, dest)

            local top3 = 0
            local score = 0
            local playerFound = false

            for i, entry in ipairs(itemScores) do
                playerFound = false

                for unit in WA_IterateGroupMembers() do
                    playerName, _ = UnitName(unit)
                    if (playerName == entry[1]) then
                        playerFound = true
                    end
                end

                if (playerFound == true) then
                    if (score ~= entry[2]) then
                        score = entry[2]
                        top3 = top3 + 1
                        if (top3 > 3) then
                            break
                        end
                    end

                    SendChatMessage(entry[1] .. " => " .. entry[2], channel, nil, dest)
                else
                    -- TODO: aggregate all top scores / player names that aren't in the raid and output them on a single line
                    --       to easily verify that we aren't missing anybody
                    print(entry[1] .. "(" .. entry[2] .. ") not in raid")
                end
            end -- for i, entry in ipairs(itemScores)

            if (i == 0) then
                SendChatMessage(" - Nobody", channel, nil, dest)
            end

            -- If request was a whispered !loot/r, let the raid know who did it
            if (event == "CHAT_MSG_WHISPER" and channel == "RAID") then
                SendChatMessage("   !loot Request by: " .. sender, channel, nil, dest)
            end
        else
            SendChatMessage(itemName .. ": not in lootlist table", channel, nil, dest)
        end
    end
end
