#!/usr/bin/env python3

#
# This file is subject to the terms and conditions defined in
# file 'LICENSE.txt', which is part of this source code package.
#

import sys, os, shutil

from itemlist import *
from rules import *
from datetime import date

import fr
import weakaura

WEAKAURA_CORE = 'https://wago.io/wW-_SK9z0'
WEAKAURA_EXPORT = 'weakaura_lootlist.txt'
CHARLIST_INDEX = 'characters.html'

def build_charlist_index(guild):
    s = """
<html>
  <head>
    <link href="export.css" rel="stylesheet" type="text/css">
  </head>
  <body>
"""

    for char in guild.members.values():
        if not char.is_alumni:
            s += '<p> <a href="characters/%s.html"> %s </a> </p>\n' % (char.name, char.name)

    s += '<p> Retired: </p>\n'
    for char in guild.members.values():
        if char.is_alumni:
            s += '<p> <a href="characters/%s.html"> %s </a> </p>\n' % (char.name, char.name)

    s += '</body> </html>'

    return s


def build_character_page(character):
    s = """
<html>
  <head>
    <link href="../export.css" rel="stylesheet" type="text/css">
  </head>
  <body>
"""

    guild = character.guild
    s += '<h3> %s </h3>\n' % (character.name)

    s += '<p> * Attendance: %.2f%% </p>\n' % (character.attendance_percentage)
    attendance_dictionnary = dict(character.attendance)
    for raidid in guild.raids:
        if raidid.week in attendance_dictionnary:
            s += "<p>    ID %d: %d </p>\n" % (raidid.week, attendance_dictionnary[raidid.week]) # For each raid ID, print attendance
        else:
            s += "<p>    ID %d: 0</p>\n" % (raidid.week)

    s += "<p> * Loots:</p>\n"
    for entry in character.acquired:
        s += "<p>    ID %d: %s [%d]</p>\n" % (entry.acquired_raid.raidid.week, entry.item.name, entry.original_score)

    s += '</body> </html>'

    return s


def build_loot_table(guild):
    tags = AQ_TAGS + NAXX_TAGS
    tag_colors = ['#dddddd', '#cccccc']
    standing_colors = ['#EE7766', '#EEA070', '#EEDC00', '#D7EE33', '#BBC4EE']

    s = "<table>\n"
    for i, tag in enumerate(tags):
        tag_items = [Item(item_name) for item_name in ITEMS if tag in ITEMS[item_name]]

        tag_items.sort(key=lambda x: x.name)
        if tag_items:
            s += "  <tr> <th rowspan=\"%d\" style=\"background-color: %s\"> %s </th> </tr>\n" % (len(tag_items) + 1, tag_colors[i % 2], tag)

        for item in tag_items:
            s += "    <tr> <th> %s </th> <th> %s </th> " % (item.name, fr.ITEMLIST_FR[item.name])
            scores = []
            for char in guild.members.values():
                if char.is_alumni:
                    continue
                item_scores = char.get_lootlist_entries(item)
                for res in item_scores:
                    scores.append((char, res))

            scores.sort(key=lambda x: x[1].score, reverse=True)

            if scores:
                last_score = scores[0][1].score

            standing = 0
            for sc in scores:
                char = sc[0]
                entry = sc[1]
                if int(entry.score) < int(last_score):
                    standing += 1

                color_index = standing if standing < len(standing_colors) else -1

                tooltip = True # Just in case we need to disable this at some point

                if tooltip:
                    # FIXME: every detail here should be exported from the rules routines
                    td = """<div class="tooltip"> %s (%d)
                    <span class="tooltiptext">
                    <p> Lootlist value: %d </p>
                    <p> Missed: %d (+%.1f) </p>
                    <p> Passed: %d (+%.1f) </p>
                    <p> Attendance: %d%% (+%.1f) </p>
                    <p> Item bonus: %+.1f </p>
                    <p> Alt: x%.2f </p>
                    </span> </div>""" % (char.name, int(entry.score),
                                         entry.original_score,
                                         entry.missed, entry.missed * 0.4,
                                         entry.passed, entry.passed * 1.0,
                                         char.attendance_percentage, char.attendance_percentage / (100.0 / MAX_ATTENDANCE_BONUS),
                                         apply_item_semantic_bonus(0, item, char.is_tank, char.is_orc_warrior or char.is_meme_warrior, char.is_orc_warrior), min(0.9, char.attendance_percentage / 100.0) if char.is_alt else 1.0)

                else:
                    td = '%s (%d)' % (char.name, entry.score)

                s += "<td style=\"background-color: %s\"> %s </td>" % (standing_colors[color_index], td)
                last_score = entry.score

            s += "</tr>\n"
    s += "</table>\n"

    return s

def export_html(guild, output_directory):

    # Create output directory
    try:
        os.mkdir(output_directory)
    except FileExistsError:
        pass
    except OSError:
        print("Failed to create %s directory" % (output_directory))
        return

    target_index_filename = os.path.join(output_directory, 'index.html')
    target_css_filename = os.path.join(output_directory, 'export.css')

    # Copy the CSS file
    shutil.copy('export.css', target_css_filename)

    #
    # Build HTML
    #

    # Build the weakaura table first so we can put the hash in the link description
    md5, wa_export_string = weakaura.generate_weakaura(guild)

    # Top area
    html_string = """
<html>
  <head>
    <link href="export.css" rel="stylesheet" type="text/css">
  </head>
  <body>
    <p> Last update: %s </p>
    <p> <a href="%s"> Weakaura v%s </a> </p>
    <p> <a href="%s"> Weakaura export list: [%s] </a> </p>
    <p> <a href="%s"> Character list </a> </p>
    """ % (str(date.today()),
           WEAKAURA_CORE, weakaura.WA_VERSION,
           WEAKAURA_EXPORT, md5,
           CHARLIST_INDEX)

    # Actual lootlist table
    html_string += build_loot_table(guild)

    # Footer
    html_string += "</body> </html>"

    # Write HTML to its file
    with open(target_index_filename, 'w') as f:
        f.write(html_string)

    #
    # Generate the weakaura exports
    #
    target_wa_filename = os.path.join(output_directory, WEAKAURA_EXPORT)

    with open(target_wa_filename, 'w') as f:
        f.write(wa_export_string)

    #
    # Generate characters index
    #
    target_charlist_index = os.path.join(output_directory, CHARLIST_INDEX)
    charlist_html_string = build_charlist_index(guild)

    with open(target_charlist_index, 'w') as f:
        f.write(charlist_html_string)

    #
    # Generate each character's page
    #
    # Create characters directory
    output_char_directory = os.path.join(output_directory, 'characters')
    try:
        os.mkdir(output_char_directory)
    except FileExistsError:
        pass
    except OSError:
        print("Failed to create %s directory" % (output_char_directory))
        return

    for char in guild.members.values():
        target_char_page = os.path.join(output_char_directory, char.name + '.html')
        char_string = build_character_page(char)

        with open(target_char_page, 'w') as f:
            f.write(char_string)
