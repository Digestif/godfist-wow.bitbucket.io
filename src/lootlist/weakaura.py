#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# This file is subject to the terms and conditions defined in
# file 'LICENSE.txt', which is part of this source code package.
#

from itemlist import *
from datetime import date

import fr
import hashlib

"""
This file generates the lootlist tables for the weakaura maintained in weakaura_core.lua.
The output of this file should be copy/pasted in the init script of your WA ingame.
"""

WA_VERSION = "0.2" # This should match the __VERSION__ in weakaura_core.lua
CORE_SCRIPT = "weakaura_core.lua"

# A dictionnary used to map ascii names used by the lootlist to the shit names people use ingame
SHIT_NAMES = {
    "Albion"  :  "Albiøn",
    "Dritz"   :  "Drìtz",
    "Gain"    :  "Gaïn",
    "Hadeis"  :  "Hadeïs",
    "Nynaeve" :  "Nynæve",
    "Paris"   :  "Parîs",
    "Piwee"   :  "Pïwee",
    "Skip"    :  "Skîp",
    "Aezog"   :  "Æzog",
}

def generate_lootlist_lua_table(guild, language):
    """Displays all standings in LUA arrays for the lootlist WA
    """

    s = "aura_env.LOOTLIST_%s = {" % (language)

    items = [Item(item_name) for item_name in ITEMS]
    for item in items:
        s += "[\"%s\"] = {" % (item.name if language == "EN" else fr.ITEMLIST_FR[item.name])

        scores = []
        for char in guild.members.values():
            if char.is_alumni:
                continue
            item_scores = char.get_lootlist_entries(item)
            for res in item_scores:
                scores.append((char, res))

            scores.sort(key=lambda x: x[1].score, reverse=True)

        for sc in scores:
            char = sc[0]
            entry = sc[1]
            s += "{'%s', %d}, " % (char.name if char.name not in SHIT_NAMES else SHIT_NAMES[char.name],
                                   entry.score)

        s += "},\n"

    s += "}\n"

    return s


def generate_weakaura(guild):

    tables = """
    -- How to update your lootlist weakaura:
    --
    -- 1) Log in your character, and type /wa to open your weakaura menu
    -- 2) Select the Godfist/Lootlist weakaura
    -- 3) In "Action" -> "On initialization", check "Custom" if it's not already checked.
    -- 4) Copy and paste everything in this page into the "Custom code" box. The game will freeze for a bit.
    -- 5) Click "accept". The game will freeze again, just wait.
    -- 6) Try typing "!loot bla"


"""
    en_table = generate_lootlist_lua_table(guild, "EN")

    # Only take the first 8 characters of the hash. Who cares.
    md5 = hashlib.md5(en_table.encode()).hexdigest()[:8]

    tables += 'aura_env.CORE_VERSION = "%s"\n' % (WA_VERSION)
    tables += 'aura_env.LOOTLIST_MD5 = "%s"\n' % (md5)
    tables += en_table
    tables += generate_lootlist_lua_table(guild, "FR")

    return md5, tables

