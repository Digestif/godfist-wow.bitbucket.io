#!/usr/bin/env python3

#
# This file is subject to the terms and conditions defined in
# file 'LICENSE.txt', which is part of this source code package.
#

# Various displays possibilities:
# 1) A simple, per-boss, loot table with priorities for each item
# 2) A detailed, per-character tracing to help with debugging and following progression
# 3) A detailed per-item tracing and priority table

from itemlist import *
from rules import *
from datetime import date

import fr

def display_scores_per_tag(guild, tags, characters, items, limit=-1):
    """Displays items ordered by tag, showing each character's score per item.

    If `tags' is empty, all items in the list are displayed in
    order. Otherwise, only items which have a matching tag will be
    displayed.
    """
    for tag in tags if tags else ["--"]:
        print(" -- %s --" % (tag))

        # If no tag is specified, list all items
        if tags:
            tag_items = [item for item in items if tag in item.tags]
        else:
            tag_items = items

        for it in tag_items:
            scores = []
            for char in characters:
                item_scores = char.get_item_scores(it)
                for s in item_scores:
                    scores.append((char, s))

            scores.sort(key=lambda x: x[1], reverse=True)

            count = limit
            if count == -1:
                count = len(scores)

            s = "| {:<35} |".format(it.name)
            if scores:
                for sc in scores[:count]:
                    s += " {:<12} ({:<3}%) |".format(sc[0].name, int(sc[0].attendance_percentage))

            print(s)

            s = "| {:<35} |".format(" ")
            if scores:
                for sc in scores[:count]:
                    s += " {:<19} |".format(int(sc[1]))

            print(s)
            print("-" * len(s))


def display_detailed_character(guild, tags, character, items):
    """Displays a single character detailed history."""
    print(" -- Character details: %s -- " % (character.name))
    print(" * Attendance: %.2f%%" % (character.attendance_percentage))
    attendance_dictionnary = dict(character.attendance)
    for raidid in guild.raids:
        if raidid.week in attendance_dictionnary:
            print("    ID %d: %d" % (raidid.week, attendance_dictionnary[raidid.week])) # For each raid ID, print attendance
        else:
            print("    ID %d: 0" % (raidid.week))

    print(" * Loots:")
    for entry in character.acquired:
        print("    ID %d: %s [%d]" % (entry.acquired_raid.raidid.week, entry.item.name, entry.original_score))

def display_detailed_item(guild, tags, characters, item):
    """Displays a single item detailed history."""
    print(" -- Item details -- ")

def display_attendance(guild, characters):
    """Displays attendance table"""
    print(" -- Attendance details -- ")
    chars = sorted(characters, key=lambda x: x.attendance_percentage, reverse=True)
    for char in chars:
        print("    %16s : %.2f%%" % (char.name, char.attendance_percentage))

def display_global_html(guild):
    """Displays an HTML summary of all standings"""
    tags = AQ_TAGS + NAXX_TAGS
    tag_colors = ['#dddddd', '#cccccc']
    standing_colors = ['#EE7766', '#EEA070', '#EEDC00', '#D7EE33', '#BBC4EE']

    s = """
<html>
  <head>
    <link href="export.css" rel="stylesheet" type="text/css">
  </head>
  <body>
    <p> Last update: %s </p>
    <table>
    """ % (str(date.today()))
    for i, tag in enumerate(tags):
        tag_items = [Item(item_name) for item_name in ITEMS if tag in ITEMS[item_name]]

        tag_items.sort(key=lambda x: x.name)
        if tag_items:
            s += "  <tr> <th rowspan=\"%d\" style=\"background-color: %s\"> %s </th> </tr>\n" % (len(tag_items) + 1, tag_colors[i % 2], tag)

        for item in tag_items:
            s += "    <tr> <th> %s </th> <th> %s </th> " % (item.name, fr.ITEMLIST_FR[item.name])
            scores = []
            for char in guild.members.values():
                if char.is_alumni:
                    continue
                item_scores = char.get_lootlist_entries(item)
                for res in item_scores:
                    scores.append((char, res))

            scores.sort(key=lambda x: x[1].score, reverse=True)

            if scores:
                last_score = scores[0][1].score

            standing = 0
            for sc in scores:
                char = sc[0]
                entry = sc[1]
                if int(entry.score) < int(last_score):
                    standing += 1

                color_index = standing if standing < len(standing_colors) else -1

                tooltip = True

                if tooltip:
                    # FIXME: every detail here should be exported from the rules routines
                    td = """<div class="tooltip"> %s (%d)
                    <span class="tooltiptext">
                    <p> Lootlist value: %d </p>
                    <p> Missed: %d (+%.1f) </p>
                    <p> Passed: %d (+%.1f) </p>
                    <p> Attendance: %d%% (+%.1f) </p>
                    <p> Item bonus: %+.1f </p>
                    <p> Alt: x%.2f </p>
                    </span> </div>""" % (char.name, int(entry.score),
                                         entry.original_score,
                                         entry.missed, entry.missed * 0.4,
                                         entry.passed, entry.passed * 1.0,
                                         char.attendance_percentage, char.attendance_percentage / (100.0 / MAX_ATTENDANCE_BONUS),
                                         apply_item_semantic_bonus(0, item, char.is_tank, char.is_orc_warrior or char.is_meme_warrior, char.is_orc_warrior), min(0.9, char.attendance_percentage / 100.0) if char.is_alt else 1.0)

                else:
                    td = '%s (%d)' % (char.name, entry.score)

                s += "<td style=\"background-color: %s\"> %s </td>" % (standing_colors[color_index], td)
                last_score = entry.score

            s += "</tr>\n"

    s += """
    </table>
  </body>
</html>
    """

    print(s)
